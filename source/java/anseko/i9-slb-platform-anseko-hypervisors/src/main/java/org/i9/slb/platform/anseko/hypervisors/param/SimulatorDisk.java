package org.i9.slb.platform.anseko.hypervisors.param;

/**
 * 虚拟化模拟器磁盘描述
 *
 * @author R12
 * @version 1.0
 * @date 2018/8/30 10:38
 */
public class SimulatorDisk {

    /**
     * 磁盘uuid
     */
    private String uuid;

    /**
     * 磁盘路径
     */
    private String diskPath;

    /**
     * 虚拟化模拟器磁盘描述构建函数
     *
     * @param uuid
     * @param diskPath
     */
    public SimulatorDisk(String uuid, String diskPath) {
        this.uuid = uuid;
        this.diskPath = diskPath;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getDiskPath() {
        return diskPath;
    }

    public void setDiskPath(String diskPath) {
        this.diskPath = diskPath;
    }
}
