package org.i9.slb.platform.anseko.vncserver.remote;

import org.i9.slb.platform.anseko.provider.IDubboVNCServerRemoteService;
import org.i9.slb.platform.anseko.provider.dto.KeyValuesDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 调用coreservice远程服务
 *
 * @author R12
 * @version 1.0
 * @date 2018/8/30 18:16
 */
@Service
public class CoreserviceRemoteService {

    @Autowired
    private IDubboVNCServerRemoteService dubboVNCServerRemoteService;

    public KeyValuesDto remoteServiceSimulatorVNCKeyValues(String simulatorId) {
        KeyValuesDto keyValuesDto = this.dubboVNCServerRemoteService.simulatorVNCKeyValues(simulatorId);
        return keyValuesDto;
    }
}
