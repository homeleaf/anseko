package org.i9.slb.platform.anseko.console.service;

import org.i9.slb.platform.anseko.hypervisors.param.SimulatorInfo;
import org.i9.slb.platform.anseko.provider.dto.SimulatorDto;

/**
 * 模拟器操作服务类
 *
 * @author R12
 * @date 2018.08.31
 */
public interface SimulatorService {

    /**
     * 创建模拟器
     *
     * @param simulatorDto
     */
    SimulatorInfo createSimulator(SimulatorDto simulatorDto);

    /**
     * 关闭模拟器
     *
     * @param simulatorDto
     */
    void shutdownSimulator(SimulatorDto simulatorDto);

    /**
     * 启动模拟器
     *
     * @param simulatorDto
     */
    void startSimulator(SimulatorDto simulatorDto);

    /**
     * 重启模拟器
     *
     * @param simulatorDto
     */
    void rebootSimulator(SimulatorDto simulatorDto);

    /**
     * 销毁模拟器
     *
     * @param simulatorDto
     */
    void destroySimulator(SimulatorDto simulatorDto);
}
