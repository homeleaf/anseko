/*
Navicat MySQL Data Transfer

Source Server         : 103.248.102.5_10069
Source Server Version : 50628
Source Host           : 103.248.102.5:10029
Source Database       : dubbo

Target Server Type    : MYSQL
Target Server Version : 50628
File Encoding         : 65001

Date: 2018-08-31 11:17:43
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for t_command_dispatch
-- ----------------------------
DROP TABLE IF EXISTS `t_command_dispatch`;
CREATE TABLE `t_command_dispatch` (
  `id` varchar(255) NOT NULL,
  `commandGroupId` varchar(255) DEFAULT NULL,
  `simulatorName` varchar(255) DEFAULT NULL,
  `startTime` datetime DEFAULT NULL,
  `endTime` datetime DEFAULT NULL,
  `success` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for t_command_execute
-- ----------------------------
DROP TABLE IF EXISTS `t_command_execute`;
CREATE TABLE `t_command_execute` (
  `id` varchar(255) NOT NULL,
  `commandGroupId` varchar(255) DEFAULT NULL,
  `commandId` varchar(255) DEFAULT NULL,
  `commandLine` varchar(255) DEFAULT NULL,
  `startTime` datetime DEFAULT NULL,
  `endTime` datetime DEFAULT NULL,
  `commandResult` varchar(255) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
