package org.i9.slb.platform.anseko.provider.service;

import org.i9.slb.platform.anseko.provider.IDubboVNCServerRemoteService;
import org.i9.slb.platform.anseko.provider.dto.InstanceDto;
import org.i9.slb.platform.anseko.provider.dto.KeyValuesDto;
import org.i9.slb.platform.anseko.provider.dto.SimulatorDto;
import org.i9.slb.platform.anseko.provider.repository.InstanceRepository;
import org.i9.slb.platform.anseko.provider.repository.SimulatorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * vncserver远程调用服务
 *
 * @author R12
 * @version 1.0
 * @date 2018/8/31 13:48
 */
@Service("dubboVNCServerRemoteService")
public class DubboVNCServerRemoteService implements IDubboVNCServerRemoteService {

    @Autowired
    private SimulatorRepository simulatorRepository;

    @Autowired
    private InstanceRepository instanceRepository;

    /**
     * 获取模拟器VNC服务信息
     *
     * @param simulatorId
     * @return
     */
    @Override
    public KeyValuesDto simulatorVNCKeyValues(String simulatorId) {
        SimulatorDto simulatorDto = this.simulatorRepository.getSimulatorDtoById(simulatorId);
        KeyValuesDto keyValuesDto = new KeyValuesDto();
        keyValuesDto.setParameter("protocol", "vnc");

        InstanceDto instanceDto = this.instanceRepository.getInstanceDtoInfo(simulatorDto.getInstance());
        keyValuesDto.setParameter("hostname", instanceDto.getRemoteAddress());
        keyValuesDto.setParameter("port", String.valueOf(simulatorDto.getVncport()));
        keyValuesDto.setParameter("password", simulatorDto.getVncpassword());
        return keyValuesDto;
    }
}
