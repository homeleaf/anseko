package org.i9.slb.platform.anseko.provider;

import com.alibaba.dubbo.container.Main;

/**
 * 服务启动主类
 *
 * @author R12
 * @date 2018.08.30
 */
public class ServerRun {

    public static void main(String[] args) {
        Main.main(args);
    }
}
