package org.i9.slb.platform.anseko.provider;

import org.i9.slb.platform.anseko.provider.dto.CommandDispatchDto;
import org.i9.slb.platform.anseko.provider.dto.CommandExecuteDto;
import org.i9.slb.platform.anseko.provider.dto.InstanceDto;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

/**
 * jdbctemplate测试类
 *
 * @author R12
 * @version 1.0
 * @date 2018/8/31 09:55
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:dubbo-provider.xml"})
@Ignore
public class JdbcTemplateTest {

    @Autowired
    private IDubboCommandRemoteService dubboCommandRemoteService;

    @Autowired
    private IDubboSimulatorRemoteService dubboSimulatorRemoteService;

    @Autowired
    private IDubboInstanceRemoteService dubboInstanceRemoteService;

    @Test
    public void testDubboCommandRemoteService() {
        CommandDispatchDto commandDispatchDto = new CommandDispatchDto();
        commandDispatchDto.setCommandGroupId(UUID.randomUUID().toString());
        commandDispatchDto.setSimulatorId("android.003.idc9000.com");

        List<CommandExecuteDto> commandExecuteDtos = new ArrayList<CommandExecuteDto>();
        for (int i = 0; i < 10; i++) {
            CommandExecuteDto commandExecuteDto = new CommandExecuteDto();
            commandExecuteDto.setCommandLine("dd if=/dev/zero of=/tmp/data.img bs=1G count=100");
            commandExecuteDto.setCommandId(UUID.randomUUID().toString());
            commandExecuteDtos.add(commandExecuteDto);
        }

        commandDispatchDto.setCommandExecuteDtos(commandExecuteDtos);
        dubboCommandRemoteService.launchCommandDispatch(commandDispatchDto);

        for (CommandExecuteDto commandExecuteDto : commandDispatchDto.getCommandExecuteDtos()) {
            HashMap<String, Object> param = new HashMap<String, Object>();
            param.put("commandId", commandExecuteDto.getCommandId());
            param.put("commandResult", "success");
            dubboCommandRemoteService.callbackCommandExecute(param);
        }
    }

    @Test
    public void testCreateSimulatorInfo() {
        for (int i = 0; i < 50; i++) {
            String simulatorName = "android." + i + ".idc9000.com";
            dubboSimulatorRemoteService.createSimulatorInfo(simulatorName, "61.181.128.231");
        }
    }

    @Test
    public void testCreateInstanceInfo() {
        InstanceDto instanceDto = new InstanceDto();
        instanceDto.setVirtualType(1);
        instanceDto.setRemoteAddress("61.181.128.231");
        instanceDto.setInstanceName("实例主机1");
        instanceDto.setId(UUID.randomUUID().toString());
        this.dubboInstanceRemoteService.createInstanceInfo(instanceDto);
    }
}
