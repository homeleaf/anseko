package org.i9.slb.platform.anseko.downstream;

import org.i9.slb.platform.anseko.downstream.dto.param.FileCommandParamDto;

import java.util.List;

/**
 * file命令远程服务调用
 *
 * @author R12
 * @date 2018.08.29
 */
public interface IFileCommandExecuteRemoteService {

    void fileCommandExecute(FileCommandParamDto fileCommandParamDto);

    void fileCommandExecuteBatch(List<FileCommandParamDto> fileCommandParamDtos);

}
